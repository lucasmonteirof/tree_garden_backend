class CreatePlantedTrees < ActiveRecord::Migration[5.1]
  def change
    create_table :planted_trees do |t|
      t.references :garden, foreign_key: true
      t.references :tree, foreign_key: true
      t.references :park, foreign_key: true

      t.timestamps
    end
  end
end
