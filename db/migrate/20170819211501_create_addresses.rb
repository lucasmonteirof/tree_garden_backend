class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :state
      t.string :city
      t.string :zip_code
      t.string :street_name
      t.string :neighborhood
      t.string :complement
      t.integer :house_number

      t.timestamps
    end
  end
end
