class AddParkToAddress < ActiveRecord::Migration[5.1]
  def change
    add_reference :addresses, :park, foreign_key: true
  end
end
