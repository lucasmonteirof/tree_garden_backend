class AddXAndYToPlantedTrees < ActiveRecord::Migration[5.1]
  def change
    add_column :planted_trees, :x, :integer, default: 0
    add_column :planted_trees, :y, :integer, default: 0
  end
end
