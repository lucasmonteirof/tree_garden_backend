class Park < ApplicationRecord
  has_one :address, dependent: :destroy
  has_many :planted_trees
  has_many :trees, through: :planted_trees

  accepts_nested_attributes_for :address

  validates_presence_of :name, :address
end
