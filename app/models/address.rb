class Address < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :park, optional: true

  validates_presence_of :state, :city, :zip_code, :street_name, :neighborhood,
    :house_number
end
