class Garden < ApplicationRecord
  has_many :planted_trees
  has_many :trees, through: :planted_trees
  belongs_to :user

  validates_presence_of :name
  validates_uniqueness_of :name
end
