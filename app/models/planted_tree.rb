class PlantedTree < ApplicationRecord
  belongs_to :garden
  belongs_to :tree
  belongs_to :park

  validates_presence_of :garden_id, :tree_id, :park_id, :x, :y
end
