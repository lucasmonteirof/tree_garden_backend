class Tree < ApplicationRecord
  has_many :planted_trees
  has_many :gardens, through: :planted_trees
  has_many :parks, through: :planted_trees

  validates_presence_of :name, :description, :price
end
