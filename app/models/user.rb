class User < ApplicationRecord
  has_one :address, dependent: :destroy
  has_one :garden

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :garden

  validates_uniqueness_of :cpf
  validates_presence_of :name, :lastname, :birthday, :cpf, :phone,
    :address, :garden, :points

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_token_authenticatable
end
