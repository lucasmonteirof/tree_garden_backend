class ParksController < ApplicationController
  before_action :set_park, only: [:show, :update, :destroy]

  def index
    @parks = Park.all
    render json: @parks, status: :ok
  end

  def show
    render json: @park, status: :ok
  end

  def create
    @park = Park.new(park_params)
    if @park.save
      render json: @park, status: :created
    else
      render json: @park, status: :unprocessable_entity
    end
  end

  def update
    if @park.update(park_params)
      render json: @park, status: :ok
    else
      render json: @park, status: :unprocessable_entity
    end
  end

  def destroy
    if @park.destroy
      render json: @park, status: :ok
    else
      render json: @park, status: :unprocessable_entity
    end
  end

  private

  def set_park
    @park = Park.find(params[:id])
  end

  def park_params
    params.require(:park).permit(
      :name,
      address_attributes: [
        :id,
        :state,
        :city,
        :zip_code,
        :street_name,
        :neighborhood,
        :complement,
        :house_number,
        :user_id,
        :park_id
      ]
    )
  end
end
