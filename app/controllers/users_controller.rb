class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy, :redeem_points, :plant_tree]
  before_action :set_garden, only: [:garden, :update_garden, :planted_trees]

  def index
    @users = User.all
    render json: @users, status: :ok
  end

  def show
    render json: @user, include: [:address, :garden], status: :ok
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, include: [:address, :garden], status: :created
    else
      render json: @user, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      render json: @user, include: [:address, :garden], status: :ok
    else
      render json: @user, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.destroy
      render json: @user, status: :ok
    else
      render json: @user, status: :unprocessable_entity
    end
  end

  # User related

  def redeem_points
    if code_params[:code] == "NATURA2017"
      @user.increment(:points, by = 500)
      if @user.save
        render json: @user, include: [:address, :garden], status: :ok
      else
        byebug
        render json: @user, status: :unprocessable_entity
      end
    else
      render json: { error: "The submitted code is invalid" }, status: :forbidden
    end
  end

  # Garden related

  def garden
    render json: @garden, include: [:trees], status: :ok
  end

  def update_garden
    if @garden.update(garden_params)
      render json: @garden, status: :ok
    else
      render json: @garden, status: :unprocessable_entity
    end
  end

  # Trees related

  def planted_trees
    @planted_trees = PlantedTree.where(garden_id: @garden.id)
    render json: @planted_trees, status: :ok
  end

  def plant_tree
    @tree = Tree.find(tree_plantation_params[:tree_id])
    if @user.points >= @tree.price
      @planted_tree = PlantedTree.new(tree_plantation_params)

      if PlantedTree.any?
        @planted_tree.id = PlantedTree.last.id + 1
      else
        @planted_tree.id = 1
      end

      @planted_tree.garden = @user.garden
      if @planted_tree.save
        @user.decrement(:points, by = @tree.price)
        if @user.save
          render json: @planted_tree, status: :created
        else
          render json: @planted_tree, status: :unprocessable_entity
        end
      else
        render json: @planted_tree, status: :unprocessable_entity
      end
    else
      render json: { error: "You don't have enough points to buy this tree" }, status: :forbidden
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_garden
    @garden = Garden.where(id: params[:id]).first
  end

  def user_params
    params.require(:user).permit(
      :id,
      :name,
      :lastname,
      :birthday,
      :points,
      :cpf,
      :phone,
      :email,
      :password,
      :password_confirmation,
      address_attributes: [
        :id,
        :state,
        :city,
        :zip_code,
        :street_name,
        :neighborhood,
        :complement,
        :house_number,
        :user_id,
        :park_id
      ],
      garden_attributes: [:id, :name]
    )
  end

  def garden_params
    params.require(:garden).permit(:id, :name)
  end

  def tree_plantation_params
    params.permit(
      :id,
      :tree_id,
      :park_id,
      :x,
      :y
    )
  end

  def code_params
    params.permit(:id, :code)
  end
end
