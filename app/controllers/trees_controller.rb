class TreesController < ApplicationController
  before_action :set_tree, only: [:show, :update, :destroy]

  def index
    @trees = Tree.all
    render json: @trees, status: :ok
  end

  def show
    render json: @tree, status: :ok
  end

  def create
    @tree = Tree.new(tree_params)
    if @tree.save
      render json: @tree, status: :created
    else
      render json: @tree, status: :unprocessable_entity
    end
  end

  def update
    if @tree.update(tree_params)
      render json: @tree, status: :ok
    else
      render json: @tree, status: :unprocessable_entity
    end
  end

  def destroy
    if @tree.destroy
      render json: @tree, status: :ok
    else
      render json: @tree, status: :unprocessable_entity
    end
  end

  private

  def set_tree
    @tree = Tree.find(params[:id])
  end

  def tree_params
    params.require(:tree).permit(:name, :description, :price)
  end
end
