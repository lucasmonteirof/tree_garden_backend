ActiveAdmin.register PlantedTree do

  index do
    column :id
    column :owner do |planted_tree|
      "#{planted_tree.garden.user.name} #{planted_tree.garden.user.lastname}"
    end
    column :tree
    column :garden
    column :park
    column :created_at

    actions
  end

  show title: proc{ |planted_tree| "#{planted_tree.garden.user.name}'s Tree" }  do
    attributes_table do
      row :id
      row :owner do |planted_tree|
        "#{planted_tree.garden.user.name} #{planted_tree.garden.user.lastname}"
      end
      row :tree
      row :garden
      row :park
      row :x
      row :y
      row :created_at
    end
  end

  form do |f|
    f.inputs "Planted Tree Details" do
      f.input :tree
      f.input :garden
      f.input :park
      f.input :x
      f.input :y
    end

    actions
  end

  controller do
    def permitted_params
      params.permit planted_tree: [
        :user_ud,
        :garden_id,
        :tree_id,
        :park_id,
        :x,
        :y
      ]
    end
  end
end
