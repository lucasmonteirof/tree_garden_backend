ActiveAdmin.register Tree do

  filter :id
  filter :parks
  filter :name
  filter :description
  filter :price
  filter :created_at

  controller do
    def permitted_params
      params.permit tree: [:name, :description, :price, :image_url]
    end
  end
end
