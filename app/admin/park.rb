ActiveAdmin.register Park do

  filter :id
  filter :trees
  filter :name
  filter :created_at

  index do
    column :id
    column :name
    column :our_trees do |park|
      park.trees.size
    end
    column :state do |park|
      park.address.state
    end
    column :city do |park|
      park.address.city
    end
    column :neighborhood do |park|
      park.address.neighborhood
    end
    column :street_name do |park|
      park.address.street_name
    end
    column :created_at

    actions
  end

  show title: proc{ |park| park.name } do
    attributes_table do
      row :id
      row :name
      row :our_trees do |park|
        park.trees.size
      end
      row :state do |park|
        park.address.state
      end
      row :city do |park|
        park.address.city
      end
      row :zip_code do |park|
        park.address.zip_code
      end
      row :street_name do |park|
        park.address.street_name
      end
      row :neighborhood do |park|
        park.address.neighborhood
      end
      row :complement do |park|
        park.address.complement
      end
      row :house_number do |park|
        park.address.house_number
      end
      row :created_at
    end
  end

  form do |f|
    f.inputs 'Park Details' do
      f.input :name
      f.has_many :address, new_record: false do |a|
        a.input :state
        a.input :city
        a.input :zip_code
        a.input :street_name
        a.input :neighborhood
        a.input :complement
        a.input :house_number
      end

      actions
    end
  end

  controller do
    def permitted_params
      params.permit park: [
        :name,
        address_attributes: [
          :id,
          :state,
          :city,
          :zip_code,
          :street_name,
          :neighborhood,
          :complement,
          :house_number,
          :user_id,
          :park_id
        ]
      ]
    end
  end
end
