ActiveAdmin.register User do

  filter :id
  filter :name
  filter :lastname
  filter :email
  filter :points
  filter :cpf
  filter :phone

  index do
    column :id
    column :name
    column :lastname
    column :email
    column :points
    column :birthday
    column "CPF", :cpf
    column :phone
    column :state do |user|
      user.address.state
    end
    column :city do |user|
      user.address.city
    end
    column :garden
    column :trees do |user|
      user.garden.trees.size
    end
    column :created_at

    actions
  end

  show title: proc{ |user| "#{user.name} #{user.lastname}" } do
    attributes_table do
      row :id
      row :name
      row :lastname
      row :email
      row :points
      row :birthday
      row "CPF" do |user|
        user.cpf
      end
      row :phone
      row :state do |user|
        user.address.state
      end
      row :city do |user|
        user.address.city
      end
      row :zip_code do |user|
        user.address.zip_code
      end
      row :street_name do |user|
        user.address.street_name
      end
      row :neighborhood do |user|
        user.address.neighborhood
      end
      row :complement do |user|
        user.address.complement
      end
      row :house_number do |user|
        user.address.house_number
      end
      row :garden
    end
  end

  form do |f|
    f.inputs 'User Details' do
      f.input :name
      f.input :lastname
      f.input :email
      f.input :birthday
      f.input :cpf, label: 'CPF'
      f.input :phone
      f.has_many :address, new_record: false do |a|
        a.input :state
        a.input :city
        a.input :zip_code
        a.input :street_name
        a.input :neighborhood
        a.input :complement
        a.input :house_number
      end
      f.has_many :garden, new_record: false do |g|
        g.input :name
      end

      actions
    end
  end

  controller do
    def permitted_params
      params.permit user: [
        :name,
        :lastname,
        :birthday,
        :cpf,
        :phone,
        :email,
        :password,
        :password_confirmation,
        address_attributes: [
          :id,
          :state,
          :city,
          :zip_code,
          :street_name,
          :neighborhood,
          :complement,
          :house_number,
          :user_id,
          :park_id
        ],
        garden_attributes: [:id, :name]
      ]
    end
  end
end
