# Tree Garden - BackEnd

## Built With
This project was built using the following open source components:

- [Ruby](https://github.com/ruby/ruby) (2.4.1)
- [Ruby on Rails](https://github.com/rails/rails) (~> 5.1.3)
- [SQLite3](https://github.com/mackyle/sqlite) (1.3.13)
- [RSpec Rails](https://github.com/rspec/rspec-rails) (~> 3.5.4)
- [Factory Girl Rails](https://github.com/thoughtbot/factory_girl_rails) (~> 4.8.0)

Check out the live version running on Heroku: https://treegarden-backend.herokuapp.com/

## API Documentation

### Entities

Routes: https://imgur.com/a/gMc7q

##### /users
```json
{
    "user": {
      "email": "test@example.com",
      "password": "password",
      "password_confirmation": "password",
      "name": "John",
      "lastname": "Smith",
      "birthday": "25/12/1990",
      "cpf": "123.456.789-00",
      "phone": "(11) 912345-7890",
      "garden_attributes": {
        "name": "My Cool Garden"
      },
      "address_attributes":{
        "state": "SP",
        "city": "S? Paulo",
        "zip_code": "03215-001",
        "street_name": "Rua Almeida",
        "neighborhood": "Campo Belo",
        "house_number": 100
      }
    }
  }
```

##### /trees
```json
{
    "tree": {
        "name": "Pineapple Tree",
        "image_url": "https://www.iconexperience.com/_img/v_collection_png/512x512/shadow/tree.png",
        "description": "Lorem ipsum dolor amet.",
        "price": 500
    }
}
```

##### /parks
```json
{
  "park": {
        "name": "Central Park",
        "address_attributes": {
          "state": "SP",
            "city": "Sao Paulo",
            "zip_code": "01538-001",
            "street_name": "Av. Lins de Vasconcelos",
            "neighborhood": "Cambuci",
            "complement": "Numbers 1222 and 1264",
          "house_number": 1222
        }
    }
}
```

---

### Actions

#### Index
URL: /entity

Method: GET

#### Show
URL: /entity/**:id**

Method: GET

#### Create
URL: /entity

Method: POST

#### Update
URL: /entity/**:id**

Method: PUT

#### Destroy
URL: /entity/**:id**

Method: DELETE

### Extra Actions

#### Login and Logout
URL: /sessions

Methods: POST, DELETE

```json
{
    "email": "email@example.com",
    "password": "password"
}
```

#### Garden and Trees
URL: /users/**:id**/garden

Method: GET

#### Redeem Points
URL: /users/**:id**/redeem_points

Method: POST

```json
{
  "code": "NATURA2017"
}
```

#### Plant Tree
URL: /users/**:id**/garden/plant_tree

Method: POST

```json
{
  "tree_id": 1,
  "park_id": 1,
  "x": 25,
  "y": 33
}
```
