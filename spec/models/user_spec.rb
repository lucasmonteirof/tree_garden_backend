require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :lastname }
    it { is_expected.to validate_presence_of :birthday }
    it { is_expected.to validate_presence_of :cpf }
    it { is_expected.to validate_presence_of :phone }
    it { is_expected.to validate_presence_of :address }
    it { is_expected.to validate_presence_of :email }
    it { is_expected.to validate_presence_of :password }
    it { is_expected.to validate_presence_of :points }

    it { is_expected.to validate_uniqueness_of :cpf }

    it { is_expected.to have_one :address }
    it { is_expected.to have_one :garden }
  end
end
