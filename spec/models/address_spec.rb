require 'rails_helper'

RSpec.describe Address, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :state }
    it { is_expected.to validate_presence_of :city }
    it { is_expected.to validate_presence_of :zip_code }
    it { is_expected.to validate_presence_of :street_name }
    it { is_expected.to validate_presence_of :neighborhood }
    it { is_expected.to validate_presence_of :house_number }

    it { is_expected.to_not validate_presence_of :complement }

    it { is_expected.to belong_to :user }
    it { is_expected.to belong_to :park }
  end
end
