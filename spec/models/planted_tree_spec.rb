require 'rails_helper'

RSpec.describe PlantedTree, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of :x }
    it { is_expected.to validate_presence_of :y }
    it { is_expected.to validate_presence_of :tree_id }
    it { is_expected.to validate_presence_of :garden_id }
    it { is_expected.to validate_presence_of :park_id }

    it { is_expected.to belong_to :tree }
    it { is_expected.to belong_to :garden }
    it { is_expected.to belong_to :park }
  end
end
