require 'rails_helper'

RSpec.describe Garden, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_uniqueness_of :name }

    it { should have_many(:trees).through(:planted_trees) }
    it { is_expected.to have_many :planted_trees }
    it { is_expected.to belong_to :user }
  end
end
