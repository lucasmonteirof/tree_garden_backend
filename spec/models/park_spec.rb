require 'rails_helper'

RSpec.describe Park, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :address }

    it { is_expected.to have_one :address }
    it { should have_many(:trees).through(:planted_trees) }
    it { is_expected.to have_many :planted_trees }
  end
end
