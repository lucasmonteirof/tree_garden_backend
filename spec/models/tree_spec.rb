require 'rails_helper'

RSpec.describe Tree, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :description }
    it { is_expected.to validate_presence_of :price }

    it { should have_many(:gardens).through(:planted_trees) }
    it { should have_many(:parks).through(:planted_trees) }
    it { is_expected.to have_many :planted_trees }
  end
end
