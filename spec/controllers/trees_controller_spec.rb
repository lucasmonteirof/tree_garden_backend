require 'rails_helper'

RSpec.describe TreesController, type: :controller do
  describe "GET 'index'" do
    before do
      FactoryGirl.create_list(:tree, 5)
    end

    it "returns an 'ok' (200) http status code" do
      get :index
      expect(response.status).to eq(200)
    end

    it 'returns a list of all trees' do
      get :index
      expect(JSON.parse(response.body).count).to match(Tree.count)
    end
  end

  describe "GET 'show'" do
    let(:tree) { FactoryGirl.create(:tree) }

    it "returns an 'ok' (200) http status code" do
      get :show, params: { id: tree.id }
      expect(response.status).to eq(200)
    end

    it "returns the correct tree" do
      get :show, params: { id: tree.id }
      expect(JSON.parse(response.body)['id']).to eq(tree.id)
    end
  end

  describe "POST 'create'" do
    let(:params) {
      {
        tree: {
          name: 'Japanese Red Pine',
          description: 'Perhaps the most common Japanese tree.',
          price: 500
        }
      }
    }

    it "returns a 'created' (201) http status code" do
      post :create, params: params
      expect(response.status).to eq(201)
    end

    it 'returns a tree with the setted attributes' do
      post :create, params: params
      expect(Tree.last.attributes).to include({
        'name' => 'Japanese Red Pine',
        'description' => 'Perhaps the most common Japanese tree.',
        'price' => 500
      })
    end

    it 'adds 1 tree to the database' do
      expect { post :create, params: params }.to change { Tree.count }.by(1)
    end

    context "with invalid parameters" do
      let(:params) { { tree: { name: '' } } }

      it "returns a 'unprocessable_entity' (422) http status code" do
        post :create, params: params
        expect(response.status).to eq(422)
      end

      it "does not affect the database" do
        expect { post :create, params: params }.to_not change { Tree.count }
      end
    end
  end

  describe "PUT 'update'" do
    let(:tree) { FactoryGirl.create(:tree) }
    let(:params) {
      {
        id: tree.id,
        tree: {
          name: 'Pine Tree',
          price: 300
        }
      }
    }

    it "returns an 'ok' (200) http status code" do
      put :update, params: params
      expect(response.status).to eq(200)
    end

    it "changes the tree's name to 'Pine Tree'" do
      put :update, params: params
      tree.reload
      expect(tree.name).to eq('Pine Tree')
    end

    it "changes the tree's price to '300'" do
      put :update, params: params
      tree.reload
      expect(tree.price).to eq(300)
    end
  end

  describe "DELETE 'destroy'" do
    let!(:tree) { FactoryGirl.create(:tree) }

    it "returns an 'ok' (200) http status code" do
      delete :destroy, params: { id: tree.id }
      expect(response.status).to eq(200)
    end

    it "removes a tree form the database" do
      expect { delete :destroy, params: { id: tree.id } }
        .to change { Tree.count }.by(-1)
    end
  end
end
