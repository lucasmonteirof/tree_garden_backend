require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "GET 'index'" do
    before do
      FactoryGirl.create(:user)
    end

    it "returns an 'ok' (200) http status code" do
      get :index
      expect(response.status).to eq(200)
    end

    it 'returns a list of all users' do
      get :index
      expect(JSON.parse(response.body).count).to match(User.count)
    end
  end

  describe "GET 'show'" do
    let(:user) { FactoryGirl.create(:user) }

    it "returns an 'ok' (200) http status code" do
      get :show, params: { id: user.id }
      expect(response.status).to eq(200)
    end

    it "returns the correct user" do
      get :show, params: { id: user.id }
      expect(JSON.parse(response.body)['id']).to eq(user.id)
    end
  end

  describe "POST 'create'" do
    let(:params) {
      {
        user: {
          email: "test@example.com",
          password: "password",
          password_confirmation: "password",
          name: "John",
          lastname: "Smith",
          birthday: "25/12/1990",
          cpf: "123456789",
          phone: "11938493247",
          garden_attributes: {
            name: "My Cool Garden"
          },
          address_attributes:{
            state: "SP",
            city: "São Paulo",
            zip_code: "032216-111",
            street_name: "Rua Almeida",
            neighborhood: "Campo Belo",
            house_number: 242
          }
        }
      }
    }

    it "returns a 'created' (201) http status code" do
      post :create, params: params
      expect(response.status).to eq(201)
    end

    it "returns a user with the setted attributes" do
      post :create, params: params
      expect(User.last.attributes).to include({'email' => 'test@example.com'})
    end

    it "adds 1 user to the database" do
      expect { post :create, params: params }.to change { User.count }.by(1)
    end

    context "with invalid parameters" do
      let(:params) { { user: { name: '' } } }

      it "returns a 'unprocessable_entity' (422) http status code" do
        post :create, params: params
        expect(response.status).to eq(422)
      end

      it "does not affect the database" do
        expect { post :create, params: params }.to_not change { User.count }
      end
    end
  end

  describe "PUT 'update'" do
    let(:user) { FactoryGirl.create(:user) }
    let(:params) {
      {
        id: user.id,
        user: {
          name: "Mark"
        }
      }
    }

    it "returns an 'ok' (200) http status code" do
      put :update, params: params
      expect(response.status).to eq(200)
    end

    it "changes the user's name to 'Mark'" do
      put :update, params: params
      user.reload
      expect(user.name).to eq('Mark')
    end
  end

  describe "DELETE 'destroy'" do
    let!(:user) { FactoryGirl.create(:user) }

    it "returns an 'ok' (200) http status code" do
      delete :destroy, params: { id: user.id }
      expect(response.status).to eq(200)
    end

    it "removes a user form the database" do
      expect { delete :destroy, params: { id: user.id } }
        .to change { User.count }.by(-1)
    end
  end
end
