require 'rails_helper'

RSpec.describe ParksController, type: :controller do
  describe "GET 'index'" do
    before do
      FactoryGirl.create_list(:park, 5)
    end

    it "returns an 'ok' (200) http status code" do
      get :index
      expect(response.status).to eq(200)
    end

    it 'returns a list of all parks' do
      get :index
      expect(JSON.parse(response.body).count).to match(Park.count)
    end
  end

  describe "GET 'show'" do
    let(:park) { FactoryGirl.create(:park) }

    it "returns an 'ok' (200) http status code" do
      get :show, params: { id: park.id }
      expect(response.status).to eq(200)
    end

    it "returns the correct park" do
      get :show, params: { id: park.id }
      expect(JSON.parse(response.body)['id']).to eq(park.id)
    end
  end

  describe "POST 'create'" do
    let(:params) {
      {
        park: {
          name: "Central Park",
          address_attributes: {
            state: "SP",
            city: "Sao Paulo",
            zip_code: "01538-001",
            street_name: "Av. Lins de Vasconcelos",
            neighborhood: "Cambuci",
            complement: "Numbers 1222 and 1264",
            house_number: 1222
          }
        }
      }
    }

    it "returns a 'created' (201) http status code" do
      post :create, params: params
      expect(response.status).to eq(201)
    end

    it "returns a park with the setted attributes" do
      post :create, params: params
      expect(Park.last.attributes).to include({'name' => 'Central Park'})
    end

    it "adds 1 park to the database" do
      expect { post :create, params: params }.to change { Park.count }.by(1)
    end

    context "with invalid parameters" do
      let(:params) { { park: { name: '' } } }

      it "returns a 'unprocessable_entity' (422) http status code" do
        post :create, params: params
        expect(response.status).to eq(422)
      end

      it "does not affect the database" do
        expect { post :create, params: params }.to_not change { Park.count }
      end
    end
  end

  describe "PUT 'update'" do
    let(:park) { FactoryGirl.create(:park) }
    let(:params) {
      {
        id: park.id,
        park: {
          name: "Revamped Park"
        }
      }
    }

    it "returns an 'ok' (200) http status code" do
      put :update, params: params
      expect(response.status).to eq(200)
    end

    it "changes the park's name to 'Revamped Park'" do
      put :update, params: params
      park.reload
      expect(park.name).to eq('Revamped Park')
    end
  end

  describe "DELETE 'destroy'" do
    let!(:park) { FactoryGirl.create(:park) }

    it "returns an 'ok' (200) http status code" do
      delete :destroy, params: { id: park.id }
      expect(response.status).to eq(200)
    end

    it "removes a park form the database" do
      expect { delete :destroy, params: { id: park.id } }
        .to change { Park.count }.by(-1)
    end
  end
end
