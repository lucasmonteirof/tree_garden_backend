FactoryGirl.define do
  factory :planted_tree do
    association :garden, strategy: :build
    association :tree, strategy: :build
    association :park, strategy: :build
  end
end
