FactoryGirl.define do
  factory :address do
    state Faker::Address.state_abbr
    city Faker::Address.city
    zip_code Faker::Address.zip_code
    street_name Faker::Address.street_name
    neighborhood Faker::Address.community
    complement Faker::Address.secondary_address
    house_number Faker::Address.building_number
  end
end
