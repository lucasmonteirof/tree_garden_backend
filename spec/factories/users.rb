FactoryGirl.define do
  factory :user do
    name Faker::Name.name
    lastname Faker::Name.last_name
    birthday Faker::Date.birthday
    email Faker::Internet.email
    cpf "123.456.789-00"
    phone Faker::PhoneNumber.cell_phone
    association :address, strategy: :build
    association :garden, strategy: :build
    password "password"
    password_confirmation "password"
  end
end
