FactoryGirl.define do
  factory :tree do
    name "Palm Tree"
    description Faker::Lorem.sentence
    price 200
  end
end
