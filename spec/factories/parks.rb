FactoryGirl.define do
  factory :park do
    name "Central Park"
    association :address, strategy: :build
  end
end
