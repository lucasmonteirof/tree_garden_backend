Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # devise_for :users
  root to: 'trees#index'
  resources :trees, only: [:index, :show, :create, :update, :destroy]
  resources :parks, only: [:index, :show, :create, :update, :destroy]
  resources :sessions, only: [:create, :destroy]
  resources :users, only: [:index, :show, :create, :update, :destroy] do
      post 'redeem_points', action: :redeem_points, on: :member
      get 'garden', action: :garden, on: :member
      put 'garden', action: :update_garden, on: :member
      patch 'garden', action: :update_garden, on: :member
      get 'garden/trees', action: :planted_trees, on: :member
      post 'garden/plant_tree', action: :plant_tree, on: :member
  end
end
